# Online Shop 🛒
![Gitlab Pipeline Status](https://gitlab.com/heliya_darvish/online-shop/badges/main/pipeline.svg)

![java version](https://img.shields.io/badge/java-21-blue?logo=openjdk&logoColor=FF0000)
![spring version](https://img.shields.io/badge/spring--boot-3.2.2-blue?logo=spring-boot)
![spring version](https://img.shields.io/badge/maven-3.x.x-blue?logo=apachemaven&logoColor=FF0000)

This is a sample `bank-end` project for online-shop service.

## Used technologies

- ![java version](https://img.shields.io/badge/java-21-blue?logo=openjdk&logoColor=FF0000)
- spring framework
    - ![spring-boot version](https://img.shields.io/badge/spring--boot-3.2.2-blue?logo=spring-boot)
    - spring web
    - spring data-jpa
      - jpa-repository
      - jpa-auditing
      - entity-graph
      - jpa-specification
      - jpa-query-method
    - spring security
    - spring validation
    - spring actuator
    - springdoc openapi
    - spring-boot-test
- ![maven version](https://img.shields.io/badge/maven-3.x.x-blue?logo=apachemaven&logoColor=FF0000)
- ![docker](https://img.shields.io/badge/Docker-2CA5E0?style=plastic&logo=docker&logoColor=white)
- ![swagger](https://img.shields.io/badge/Swagger-85EA2D?style=plastic&logo=Swagger&logoColor=white)
- [lombok](https://projectlombok.org/)
- [mapstruct](https://mapstruct.org/)
- [h2database](https://www.h2database.com/html/main.html)
- ![mysql](https://shields.io/badge/MySQL-lightgrey?logo=mysql&style=plastic&logoColor=white&labelColor=blue)

## How to run
If you run project in `IDE` (development mode) you need to open `swagger-ui` and first of all register new user. then you can test the APIs.

For running project outside the `IDE` read the following passage.

If `hibernate-ddl` disabled you need to run `sql` scripts inside `dev` directory to generate schema.

This project is containerized using docker. The docker base-image is `eclipse-temurin:21-jre-alpine`.
The Dockerfile has a `env` variable named `JAVA_OPTS` to set java options run command. For example to set remote debug option:
```bash
docker run \
    -e JAVA_OPTS='-Xdebug -agentlib:jdwp=transport=dt_socket,address=*:8081,server=y,suspend=n' \
    -p 80:8080 \
    -p 8081:8081 \
    registry.gitlab.com/heliya_darvish/online-shop:latest
```

## Custom Config
There are custom properties which can be set in `application peoperties` file.
| name   | description   |
| ------ | ------------- |
| `app.properties.tax-percent` | tax-percent |
| `app.properties.expire-purchase-period-minute` | purchase expired after `{value}` minute |
| `app.properties.expire-purchase-schedule.rate-milliseconds`   | check purchases to expire every `{value}` milliseconds |
| `app.properties.expire-purchase-schedule.init-delay-milliseconds`   | start to check expiration of purchases `{value}` milliseconds after project startup |

## Services
- [x] authentication/authorization
    - [x] register new user
    - [x] login
    - [x] logout
- [x] products
    - [x] list
    - [x] search and filter
    - [x] get detail of single product
- [x] basket
    - [x] add item
    - [x] remove item
    - [x] clear basket (remove all items)
    - [x] get items
- [x] purchase
    - [x] buy online
    - [x] buy from wallet
    - [x] cancel
    - [x] get all not paid
- [x] wallet
    - [x] get current balance

## Security
Inorder to secure APIs, used Spring security `form-login` ConfiguredSecurityBuilder.
Security configs:
- disable CSRF
- `session-based` using `newSession` policy for SessionFixation config
- override `UserDetailsService` and connect to user entity of project
- using `BCryptPasswordEncoder` as passwordEncoder
- customize login success/failure Handler

## Purchase Service
There are two-way to pay in this project.
- online payment
    1. create purchase based on selected items in basket
    2. create online payment
    3. redirect user to `IPG`
    4. finalize payment and purchase after `IPG`
    5. clear basket
- from wallet
    - if wallet balance is enough
    - all these steps must run in single transaction
    1. create purchase based on selected items in basket and finalize it
    2. create wallet transaction `DE_CHARGE` and update wallet `balance`

If a purchase canceled, user get a refund to wallet (charge wallet).

Each `NOT_PAID` purchase expired after a while and user has not accessed to do payment for that purchase.

## Wallet Concurrency Test
Use `PESSIMISTIC_WRITE` lock for wallet concurrent change requests.

There are two test:
- sync cancel multiple purchase
- async cancel multiple purchase