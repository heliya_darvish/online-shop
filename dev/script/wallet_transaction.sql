CREATE TABLE wallet_transaction
(
    id             INT AUTO_INCREMENT NOT NULL,
    wallet_user_id INT                NOT NULL,
    purchase_id    INT                NOT NULL,
    balance        INT                NOT NULL,
    action         VARCHAR(255)       NOT NULL,
    issued_at      datetime           NOT NULL,
    CONSTRAINT pk_wallettransaction PRIMARY KEY (id)
);

ALTER TABLE wallet_transaction
    ADD CONSTRAINT FK_WALLETTRANSACTION_ON_PURCHASE
        FOREIGN KEY (purchase_id)
            REFERENCES purchase (id);

ALTER TABLE wallet_transaction
    ADD CONSTRAINT FK_WALLETTRANSACTION_ON_WALLET_USERID
        FOREIGN KEY (wallet_user_id)
            REFERENCES wallet (user_id);