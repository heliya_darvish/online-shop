CREATE TABLE basket
(
    user_id INT NOT NULL,
    CONSTRAINT pk_basket PRIMARY KEY (user_id)
);

CREATE TABLE basket_product
(
    basket_id  INT NOT NULL,
    product_id INT NOT NULL,
    CONSTRAINT pk_basket_product PRIMARY KEY (basket_id, product_id)
);

ALTER TABLE basket
    ADD CONSTRAINT FK_BASKET_ON_USER FOREIGN KEY (user_id) REFERENCES user (id);

ALTER TABLE basket_product
    ADD CONSTRAINT fk_baspro_on_basket FOREIGN KEY (basket_id) REFERENCES basket (user_id);

ALTER TABLE basket_product
    ADD CONSTRAINT fk_baspro_on_product FOREIGN KEY (product_id) REFERENCES product (id);