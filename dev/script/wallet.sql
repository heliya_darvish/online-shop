CREATE TABLE wallet
(
    user_id INT NOT NULL,
    balance INT NOT NULL,
    CONSTRAINT pk_wallet PRIMARY KEY (user_id)
);

ALTER TABLE wallet
    ADD CONSTRAINT FK_WALLET_ON_USER
        FOREIGN KEY (user_id)
            REFERENCES user (id);