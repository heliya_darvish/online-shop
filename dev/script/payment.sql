CREATE TABLE payment
(
    id          INT AUTO_INCREMENT NOT NULL,
    user_id     INT                NOT NULL,
    purchase_id INT                NOT NULL,
    status      VARCHAR(255)       NOT NULL,
    ipg_price   INT                NOT NULL,
    created_at  datetime           NOT NULL,
    modified_at datetime           NULL,
    CONSTRAINT pk_payment PRIMARY KEY (id)
);

ALTER TABLE payment
    ADD CONSTRAINT FK_PAYMENT_ON_PURCHASE FOREIGN KEY (purchase_id) REFERENCES purchase (id);

ALTER TABLE payment
    ADD CONSTRAINT FK_PAYMENT_ON_USER FOREIGN KEY (user_id) REFERENCES user (id);