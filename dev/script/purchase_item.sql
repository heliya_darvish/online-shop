CREATE TABLE purchase_item
(
    id         INT AUTO_INCREMENT NOT NULL,
    product_id INT                NOT NULL,
    price      INT                NOT NULL,
    CONSTRAINT pk_purchaseitem PRIMARY KEY (id)
);

ALTER TABLE purchase_item
    ADD CONSTRAINT FK_PURCHASEITEM_ON_PRODUCT FOREIGN KEY (product_id) REFERENCES product (id);