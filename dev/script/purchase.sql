CREATE TABLE purchase
(
    id          INT AUTO_INCREMENT NOT NULL,
    user_id     INT                NOT NULL,
    status      VARCHAR(255)       NOT NULL,
    price       INT                NOT NULL,
    tax         INT                NULL,
    created_at  datetime           NOT NULL,
    modified_at datetime           NULL,
    CONSTRAINT pk_purchase PRIMARY KEY (id)
);

CREATE TABLE purchase_purchase_item
(
    purchase_id      INT NOT NULL,
    purchase_item_id INT NOT NULL,
    CONSTRAINT pk_purchase_purchase_item PRIMARY KEY (purchase_id, purchase_item_id)
);

ALTER TABLE purchase_purchase_item
    ADD CONSTRAINT uc_purchase_purchase_item_purchase_item UNIQUE (purchase_item_id);

ALTER TABLE purchase
    ADD CONSTRAINT FK_PURCHASE_ON_USER FOREIGN KEY (user_id) REFERENCES user (id);

ALTER TABLE purchase_purchase_item
    ADD CONSTRAINT fk_purpurite_on_purchase FOREIGN KEY (purchase_id) REFERENCES purchase (id);

ALTER TABLE purchase_purchase_item
    ADD CONSTRAINT fk_purpurite_on_purchase_item FOREIGN KEY (purchase_item_id) REFERENCES purchase_item (id);