CREATE TABLE user
(
    id       INT AUTO_INCREMENT NOT NULL,
    name     VARCHAR(50)        NULL,
    username VARCHAR(30)        NOT NULL,
    password VARCHAR(60)        NOT NULL,
    CONSTRAINT pk_user PRIMARY KEY (id)
);

ALTER TABLE user
    ADD CONSTRAINT uc_user_username UNIQUE (username);