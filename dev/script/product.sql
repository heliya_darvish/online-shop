CREATE TABLE product
(
    id    INT AUTO_INCREMENT NOT NULL,
    name  VARCHAR(50)        NOT NULL,
    price INT                NOT NULL,
    type  VARCHAR(255)       NULL,
    CONSTRAINT pk_product PRIMARY KEY (id)
);