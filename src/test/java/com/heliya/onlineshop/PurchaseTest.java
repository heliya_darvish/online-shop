package com.heliya.onlineshop;

import com.heliya.onlineshop.dto.*;
import com.heliya.onlineshop.enums.PurchaseStatus;
import com.heliya.onlineshop.model.Purchase;
import com.heliya.onlineshop.model.User;
import com.heliya.onlineshop.repository.PurchaseRepository;
import com.heliya.onlineshop.security.SecurityContextUtil;
import com.heliya.onlineshop.service.*;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.Example;
import org.springframework.data.repository.query.FluentQuery;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Slf4j
@Import(AsyncConfig.class)
@TestInstance(Lifecycle.PER_CLASS)
@ActiveProfiles({"test"})
@SpringBootTest
class PurchaseTest {
    @Autowired
    PurchaseService purchaseService;
    @Autowired
    PurchaseRepository purchaseRepository;
    @Autowired
    ProductService productService;
    @Autowired
    UserService userService;
    @Autowired
    WalletService walletService;
    @Autowired
    BuyService buyService;
    @Autowired
    AsyncPurchaseCanceler asyncPurchaseCanceler;

    @BeforeAll
    void registerUser() {
        userService.register(new UserRequest("admin1", "admin1", "123456"));
        userService.register(new UserRequest("admin2", "admin2", "123456"));
    }

    @WithUserDetails(value = "admin1")
    @Test
    void syncCancelPurchase() {
        List<ProductDto> searchResult = productService.search(
                new ProductFilteredSearch("a1", null, null));

        assertEquals(1, searchResult.size());

        ProductDto product = searchResult.getFirst();
        for (int index = 0; index < 10; index++) {
            OnlinePurchaseResponse onlinePurchaseResponse = buyService
                    .submitOnlinePurchase(new PurchaseRequest(Collections.singleton(product.id())));
            buyService.finalizePayment(onlinePurchaseResponse.paymentId());
        }

        Purchase probePurchase = new Purchase();
        User user = new User();
        user.setId(SecurityContextUtil.getUserId());
        probePurchase.setUser(user);
        probePurchase.setStatus(PurchaseStatus.PAID);

        List<Purchase> paidPurchases = purchaseRepository.findBy(
                Example.of(probePurchase), FluentQuery.FetchableFluentQuery::all);

        int totalRefound = 0;
        for (Purchase purchase : paidPurchases) {
            purchaseService.cancel(purchase.getId());
            totalRefound += (purchase.getPrice() + purchase.getTax());
        }

        WalletResponse walletResponse = walletService.find();
        assertEquals(totalRefound, walletResponse.balance());
    }

    @WithUserDetails(value = "admin2")
    @Test
    void asyncCancelPurchase() {
        List<ProductDto> searchResult = productService.search(
                new ProductFilteredSearch("a1", null, null));

        assertEquals(1, searchResult.size());

        ProductDto product = searchResult.getFirst();
        for (int index = 0; index < 10; index++) {
            OnlinePurchaseResponse onlinePurchaseResponse = buyService
                    .submitOnlinePurchase(new PurchaseRequest(Collections.singleton(product.id())));
            buyService.finalizePayment(onlinePurchaseResponse.paymentId());
        }

        Purchase probePurchase = new Purchase();
        User user = new User();
        user.setId(SecurityContextUtil.getUserId());
        probePurchase.setUser(user);
        probePurchase.setStatus(PurchaseStatus.PAID);

        List<Purchase> paidPurchases = purchaseRepository.findBy(
                Example.of(probePurchase), FluentQuery.FetchableFluentQuery::all);

        int totalRefound = 0;
        List<CompletableFuture<Void>> completableFutures = new ArrayList<>(paidPurchases.size());
        for (Purchase purchase : paidPurchases) {
            completableFutures.add(asyncPurchaseCanceler.cancelPurchaseAsync(purchase));
            totalRefound += purchase.getPrice() + purchase.getTax();
        }

        int completableFutureSize = completableFutures.stream()
                .map(CompletableFuture::join)
                .toList()
                .size();

        assertEquals(paidPurchases.size(), completableFutureSize);

        WalletResponse walletResponse = walletService.find();
        assertEquals(totalRefound, walletResponse.balance());
    }

}