package com.heliya.onlineshop;

import com.heliya.onlineshop.model.Purchase;
import com.heliya.onlineshop.service.PurchaseService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.concurrent.CompletableFuture;

@Slf4j
@RequiredArgsConstructor
@Component
public class AsyncPurchaseCanceler {
    private final PurchaseService purchaseService;

    @Async
    public CompletableFuture<Void> cancelPurchaseAsync(Purchase purchase) {
        log.info("try to cancel purchase: {}", purchase.getId());
        purchaseService.cancel(purchase.getId());
        return CompletableFuture.completedFuture(null);
    }
}