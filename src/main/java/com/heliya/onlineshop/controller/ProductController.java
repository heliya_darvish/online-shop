package com.heliya.onlineshop.controller;

import com.heliya.onlineshop.dto.ProductDto;
import com.heliya.onlineshop.dto.ProductFilteredSearch;
import com.heliya.onlineshop.dto.ProductRequest;
import com.heliya.onlineshop.service.ProductService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/products")
public class ProductController {
    private final ProductService service;

    @GetMapping
    public List<ProductDto> all() {
        return service.findAll();
    }

    @PostMapping("/search")
    public List<ProductDto> search(@RequestBody @Valid ProductFilteredSearch productFilteredSearch) {
        return service.search(productFilteredSearch);
    }

    @PostMapping
    @SecurityRequirement(name = "JSESSIONID")
    public void save(@RequestBody @Valid ProductRequest productRequest) {
        service.save(productRequest);
    }

    @GetMapping("/{id}")
    public ProductDto one(@PathVariable Integer id) {
        return service.findOne(id);
    }

}
