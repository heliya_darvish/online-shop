package com.heliya.onlineshop.controller;

import com.heliya.onlineshop.dto.OnlinePurchaseResponse;
import com.heliya.onlineshop.dto.PurchaseRequest;
import com.heliya.onlineshop.enums.IpgResponse;
import com.heliya.onlineshop.service.BuyService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
public class BuyController {
    private final BuyService service;

    @PostMapping("/submit-online-purchase")
    @SecurityRequirement(name = "JSESSIONID")
    public OnlinePurchaseResponse submitOnlinePurchase(@RequestBody @Valid PurchaseRequest purchaseRequest) {
        return service.submitOnlinePurchase(purchaseRequest);
    }

    @PostMapping("/submit-online-purchase/{purchaseId}")
    @SecurityRequirement(name = "JSESSIONID")
    public OnlinePurchaseResponse submitOnlinePurchase(@PathVariable Integer purchaseId) {
        return service.submitOnlinePurchase(purchaseId);
    }

    @PostMapping("/submit-wallet-purchase")
    @SecurityRequirement(name = "JSESSIONID")
    public void submitWalletPurchase(@RequestBody @Valid PurchaseRequest purchaseRequest) {
        service.submitWalletPurchase(purchaseRequest);
    }

    @PostMapping("/submit-wallet-purchase/{purchaseId}")
    @SecurityRequirement(name = "JSESSIONID")
    public void submitWalletPurchase(@PathVariable Integer purchaseId) {
        service.submitWalletPurchase(purchaseId);
    }

    @PostMapping("/finalize-payment/{paymentId}")
    public IpgResponse finalizePayment(@PathVariable Integer paymentId) {
        return service.finalizePayment(paymentId);
    }
}