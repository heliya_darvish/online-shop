package com.heliya.onlineshop.controller;

import com.heliya.onlineshop.dto.UserRequest;
import com.heliya.onlineshop.service.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping("/users")
public class UserController {
    private final UserService service;

    @PostMapping
    public void register(@RequestBody @Valid UserRequest userRequest) {
        service.register(userRequest);
    }
}
