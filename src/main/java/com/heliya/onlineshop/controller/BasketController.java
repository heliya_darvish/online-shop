package com.heliya.onlineshop.controller;

import com.heliya.onlineshop.dto.BasketResponse;
import com.heliya.onlineshop.service.BasketService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/baskets")
public class BasketController {
    private final BasketService service;

    @GetMapping
    @SecurityRequirement(name = "JSESSIONID")
    public BasketResponse one() {
        return service.find();
    }

    @PutMapping("/add/{productId}")
    @SecurityRequirement(name = "JSESSIONID")
    public void add(@PathVariable Integer productId) {
        service.addProduct(productId);
    }

    @PutMapping("/remove/{productId}")
    @SecurityRequirement(name = "JSESSIONID")
    public void remove(@PathVariable Integer productId) {
        service.removeProduct(productId);
    }

    @PutMapping("/clear")
    @SecurityRequirement(name = "JSESSIONID")
    public void clear() {
        service.clearAfterLogin();
    }
}
