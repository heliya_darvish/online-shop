package com.heliya.onlineshop.controller;

import com.heliya.onlineshop.dto.WalletResponse;
import com.heliya.onlineshop.service.WalletService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping("/wallets")
public class WalletController {
    private final WalletService service;

    @GetMapping
    @SecurityRequirement(name = "JSESSIONID")
    public WalletResponse one() {
        return service.find();
    }

}
