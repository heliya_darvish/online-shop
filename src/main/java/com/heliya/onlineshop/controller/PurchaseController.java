package com.heliya.onlineshop.controller;

import com.heliya.onlineshop.dto.PurchaseResponse;
import com.heliya.onlineshop.service.PurchaseService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/purchase")
public class PurchaseController {
    private final PurchaseService purchaseService;

    @PostMapping("/cancel/{purchaseId}")
    @SecurityRequirement(name = "JSESSIONID")
    public void cancelPurchase(@PathVariable Integer purchaseId) {
        purchaseService.cancel(purchaseId);
    }

    @GetMapping("/all-not-paid")
    @SecurityRequirement(name = "JSESSIONID")
    public List<PurchaseResponse> allNotPaidPurchase() {
        return purchaseService.findAllNotPaidPurchase();
    }

}
