package com.heliya.onlineshop.model;

import com.heliya.onlineshop.enums.PaymentStatus;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.ZonedDateTime;

@NamedEntityGraph(
        name = "payment-purchase-entity-graph",
        attributeNodes = {
                @NamedAttributeNode("purchase")
        }
)
@EntityListeners(AuditingEntityListener.class)
@Getter
@Setter
@Entity
public class Payment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @CreatedBy
    @JoinColumn(updatable = false, nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private User user;
    @NotNull
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Purchase purchase;
    @NotNull
    @Enumerated(EnumType.STRING)
    private PaymentStatus status;
    @NotNull
    private Integer ipgPrice;
    @CreationTimestamp
    @Column(updatable = false, nullable = false)
    private ZonedDateTime createdAt;
    @UpdateTimestamp
    @Column(insertable = false)
    private ZonedDateTime modifiedAt;
}
