package com.heliya.onlineshop.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class Wallet extends AbstractManuallyAssignedIdentifierEntity<Integer> {
    @Id
    private Integer userId;
    @NotNull
    @OneToOne(fetch = FetchType.EAGER, optional = false)
    @PrimaryKeyJoinColumn
    private User user;
    private int balance;

    @Override
    public Integer getId() {
        return userId;
    }

}
