package com.heliya.onlineshop.model;


import com.heliya.onlineshop.enums.PurchaseStatus;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.ZonedDateTime;
import java.util.LinkedHashSet;
import java.util.Set;

@NamedEntityGraph(
        name = "purchase-item-entity-graph",
        attributeNodes = {
                @NamedAttributeNode("items")
        }
)
@EntityListeners(AuditingEntityListener.class)
@Getter
@Setter
@Entity
public class Purchase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @CreatedBy
    @JoinColumn(updatable = false, nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private User user;
    @NotNull
    @Enumerated(EnumType.STRING)
    private PurchaseStatus status;
    @NotNull
    private Integer price;
    private Integer tax;
    @CreationTimestamp
    @Column(updatable = false, nullable = false)
    private ZonedDateTime createdAt;
    @UpdateTimestamp
    @Column(insertable = false)
    private ZonedDateTime modifiedAt;
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinTable(
            name = "purchase_purchase_item",
            joinColumns = @JoinColumn(name = "purchase_id"),
            inverseJoinColumns = @JoinColumn(name = "purchase_item_id")
    )
    private Set<PurchaseItem> items = new LinkedHashSet<>();
}
