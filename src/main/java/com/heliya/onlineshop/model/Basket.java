package com.heliya.onlineshop.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.util.LinkedHashSet;
import java.util.Set;

@NamedEntityGraph(
        name = "basket-product-entity-graph",
        attributeNodes = {
                @NamedAttributeNode("products")
        }
)
@Getter
@Setter
@Entity
public class Basket extends AbstractManuallyAssignedIdentifierEntity<Integer> {
    @Id
    private Integer userId;
    @NotNull
    @OneToOne(fetch = FetchType.EAGER, optional = false)
    @PrimaryKeyJoinColumn
    private User user;
    @ManyToMany
    @JoinTable(
            name = "basket_product",
            joinColumns = @JoinColumn(name = "basket_id"),
            inverseJoinColumns = @JoinColumn(name = "product_id")
    )
    private Set<Product> products = new LinkedHashSet<>();

    @Override
    public Integer getId() {
        return userId;
    }

}
