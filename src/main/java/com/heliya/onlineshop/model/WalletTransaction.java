package com.heliya.onlineshop.model;

import com.heliya.onlineshop.enums.WalletTransactionAction;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import java.time.ZonedDateTime;

@Getter
@Setter
@Entity
public class WalletTransaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @NotNull
    @ManyToOne(optional = false)
    private Wallet wallet;
    @NotNull
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Purchase purchase;
    @NotNull
    private Integer balance;
    @NotNull
    @Enumerated(EnumType.STRING)
    private WalletTransactionAction action;
    @CreationTimestamp
    @Column(updatable = false, nullable = false)
    private ZonedDateTime issuedAt;
}
