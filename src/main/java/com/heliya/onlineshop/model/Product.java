package com.heliya.onlineshop.model;

import com.heliya.onlineshop.enums.ProductType;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @NotNull
    @Column(length = 50)
    private String name;
    @NotNull
    private Integer price;
    @Enumerated(EnumType.STRING)
    private ProductType type;
}
