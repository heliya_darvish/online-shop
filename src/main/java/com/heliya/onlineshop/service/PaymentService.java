package com.heliya.onlineshop.service;

import com.heliya.onlineshop.enums.PaymentStatus;
import com.heliya.onlineshop.model.Payment;
import com.heliya.onlineshop.model.Purchase;

import java.util.Set;

public interface PaymentService {

    Integer saveNewPayment(Purchase purchase);

    Payment findWithPurchaseById(Integer id);

    void update(Payment payment, PaymentStatus paymentStatus);

    void expirePaymentsByPurchaseIds(Set<Integer> purchaseIds);
}
