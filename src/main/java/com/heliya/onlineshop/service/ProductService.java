package com.heliya.onlineshop.service;

import com.heliya.onlineshop.dto.ProductDto;
import com.heliya.onlineshop.dto.ProductFilteredSearch;
import com.heliya.onlineshop.dto.ProductRequest;
import com.heliya.onlineshop.repository.projection.ProductIdAndPriceProjection;

import java.util.List;
import java.util.Set;

public interface ProductService {
    List<ProductDto> findAll();

    void save(ProductRequest productRequest);

    ProductDto findOne(Integer id);

    Set<ProductIdAndPriceProjection> findAllProductIdPriceByIds(Set<Integer> ids);

    List<ProductDto> search(ProductFilteredSearch productFilteredSearch);
}
