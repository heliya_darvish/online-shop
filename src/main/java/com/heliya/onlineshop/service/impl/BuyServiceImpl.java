package com.heliya.onlineshop.service.impl;

import com.heliya.onlineshop.dto.OnlinePurchaseResponse;
import com.heliya.onlineshop.dto.PurchaseRequest;
import com.heliya.onlineshop.enums.IpgResponse;
import com.heliya.onlineshop.enums.PaymentStatus;
import com.heliya.onlineshop.enums.PurchaseStatus;
import com.heliya.onlineshop.exception.PaymentAccessDeniedException;
import com.heliya.onlineshop.model.Payment;
import com.heliya.onlineshop.model.Purchase;
import com.heliya.onlineshop.service.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class BuyServiceImpl implements BuyService {
    private final PurchaseService purchaseService;
    private final PaymentService paymentService;
    private final WalletService walletService;
    private final BasketService basketService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public OnlinePurchaseResponse submitOnlinePurchase(PurchaseRequest purchaseRequest) {
        Purchase purchase = purchaseService.save(purchaseRequest, PurchaseStatus.NOT_PAID);
        Integer paymentId = paymentService.saveNewPayment(purchase);
        return new OnlinePurchaseResponse(paymentId);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public OnlinePurchaseResponse submitOnlinePurchase(Integer purchaseId) {
        Purchase purchase = purchaseService.findById(purchaseId);

        PurchaseStatus purchaseStatus = purchase.getStatus();
        if (!PurchaseStatus.NOT_PAID.equals(purchaseStatus))
            throw new PaymentAccessDeniedException("This purchase was " + purchaseStatus.toString().toLowerCase());

        Integer paymentId = paymentService.saveNewPayment(purchase);
        return new OnlinePurchaseResponse(paymentId);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void submitWalletPurchase(PurchaseRequest purchaseRequest) {
        Purchase purchase = purchaseService.save(purchaseRequest, PurchaseStatus.PAID);
        walletService.deChargeWallet(purchase);
        basketService.clearAfterLogin();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void submitWalletPurchase(Integer purchaseId) {
        Purchase purchase = purchaseService.findById(purchaseId);

        PurchaseStatus status = purchase.getStatus();
        if (!PurchaseStatus.NOT_PAID.equals(status))
            throw new PaymentAccessDeniedException("This purchase was " + status.toString().toLowerCase());

        walletService.deChargeWallet(purchase);
        basketService.clearAfterLogin();
    }

    @Override
    @Transactional
    public IpgResponse finalizePayment(Integer paymentId) {
        Payment payment = paymentService.findWithPurchaseById(paymentId);

        PaymentStatus paymentStatus = payment.getStatus();
        if (!PaymentStatus.INITIATED.equals(paymentStatus))
            throw new PaymentAccessDeniedException("This payment was " + paymentStatus.toString().toLowerCase());

        IpgResponse ipgResponse = callIpgApi(paymentId);
        switch (ipgResponse) {
            case SUCCESS -> {
                Purchase purchase = payment.getPurchase();
                PurchaseStatus purchaseStatus = purchase.getStatus();
                if (!PurchaseStatus.NOT_PAID.equals(purchaseStatus))
                    throw new PaymentAccessDeniedException("This purchase was " + purchaseStatus.toString().toLowerCase());

                paymentService.update(payment, PaymentStatus.SUCCESS);
                purchaseService.update(purchase, PurchaseStatus.PAID);
                basketService.clearByUserId(purchase.getUser().getId());
            }
            case FAILED -> paymentService.update(payment, PaymentStatus.FAILED);
            default -> throw new IllegalStateException("Unexpected value: " + ipgResponse);
        }
        return ipgResponse;
    }

    private IpgResponse callIpgApi(Integer paymentId) {
        //todo call mock ipg
        return IpgResponse.SUCCESS;
    }

}
