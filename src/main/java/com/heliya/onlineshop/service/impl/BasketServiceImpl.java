package com.heliya.onlineshop.service.impl;

import com.heliya.onlineshop.dto.BasketResponse;
import com.heliya.onlineshop.mapper.BasketMapper;
import com.heliya.onlineshop.model.Basket;
import com.heliya.onlineshop.model.Product;
import com.heliya.onlineshop.repository.BasketRepository;
import com.heliya.onlineshop.security.SecurityContextUtil;
import com.heliya.onlineshop.service.BasketService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.util.Collections;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class BasketServiceImpl implements BasketService {
    private final BasketRepository repository;
    private final BasketMapper mapper;

    @Override
    public BasketResponse find() {
        Integer userId = SecurityContextUtil.getUserId();
        Optional<Basket> basket = repository.findByUserId(userId);
        if (basket.isEmpty())
            return new BasketResponse(Collections.emptySet());

        return mapper.toBasketResponse(basket.get());
    }

    @Override
    @Transactional
    public void addProduct(Integer productId) {
        Integer userId = SecurityContextUtil.getUserId();
        Optional<Basket> optionalBasket = repository.findByUserId(userId);
        optionalBasket.ifPresentOrElse(basket ->
                        addProductToExistedBasket(basket, productId),
                () -> createBasketWithProduct(userId, productId)
        );
    }

    private void addProductToExistedBasket(Basket basket, Integer productId) {
        Set<Product> existedProducts = basket.getProducts();
        if (existedProducts == null) {
            addProductToEmptyBasket(basket, productId);
            return;
        }

        boolean noneMatchNewProductOnBasket = existedProducts.stream()
                .map(Product::getId)
                .noneMatch(existedProductId -> Objects.equals(existedProductId, productId));

        if (noneMatchNewProductOnBasket) {
            Product product = mapper.toProduct(productId);
            existedProducts.add(product);
            repository.save(basket);
        }
    }

    private void addProductToEmptyBasket(Basket basket, Integer productId) {
        Product product = mapper.toProduct(productId);
        basket.setProducts(Set.of(product));
        repository.save(basket);
    }

    private void createBasketWithProduct(Integer userId, Integer productId) {
        Basket basket = mapper.toBasket(userId, productId);
        repository.save(basket);
    }

    @Override
    @Transactional
    public void removeProduct(Integer productId) {
        Integer userId = SecurityContextUtil.getUserId();
        Optional<Basket> optionalBasket = repository.findByUserId(userId);
        optionalBasket.ifPresent(basket ->
                removeProductFromBasket(basket, productId));
    }

    private void removeProductFromBasket(Basket basket, Integer productId) {
        Set<Product> products = basket.getProducts();
        if (ObjectUtils.isEmpty(products))
            return;

        products.removeIf(product -> Objects.equals(product.getId(), productId));
        repository.save(basket);
    }

    @Override
    @Transactional
    public void clearAfterLogin() {
        Integer userId = SecurityContextUtil.getUserId();
        clearByUserId(userId);
    }

    @Override
    @Transactional
    public void clearByUserId(Integer userId) {
        Optional<Basket> optionalBasket = repository.findByUserId(userId);
        optionalBasket.ifPresent(basket -> {
            Set<Product> products = basket.getProducts();
            if (products != null) {
                products.clear();
                repository.save(basket);
            }
        });
    }

}
