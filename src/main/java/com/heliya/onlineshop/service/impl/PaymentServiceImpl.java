package com.heliya.onlineshop.service.impl;

import com.heliya.onlineshop.enums.PaymentStatus;
import com.heliya.onlineshop.mapper.PaymentMapper;
import com.heliya.onlineshop.model.Payment;
import com.heliya.onlineshop.model.Purchase;
import com.heliya.onlineshop.repository.PaymentRepository;
import com.heliya.onlineshop.service.PaymentService;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class PaymentServiceImpl implements PaymentService {
    private final PaymentRepository repository;
    private final PaymentMapper mapper;

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public Integer saveNewPayment(Purchase purchase) {
        Payment payment = mapper.toPayment(purchase, PaymentStatus.INITIATED);
        return repository.save(payment).getId();
    }

    @Override
    @Transactional(readOnly = true)
    public Payment findWithPurchaseById(Integer id) {
        return repository.findWithPurchaseById(id)
                .orElseThrow(() -> new EntityNotFoundException("Not found payment by id: " + id));
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public void update(Payment payment, PaymentStatus paymentStatus) {
        payment.setStatus(paymentStatus);
        repository.save(payment);
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public void expirePaymentsByPurchaseIds(Set<Integer> purchaseIds) {
        List<Payment> shouldExpiredPayments = repository.findAllByPurchaseIdInAndStatus(
                purchaseIds, PaymentStatus.INITIATED);

        shouldExpiredPayments.forEach(payment -> payment.setStatus(PaymentStatus.EXPIRED));
        repository.saveAll(shouldExpiredPayments);
    }
}
