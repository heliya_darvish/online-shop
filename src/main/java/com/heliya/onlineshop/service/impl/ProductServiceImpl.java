package com.heliya.onlineshop.service.impl;

import com.heliya.onlineshop.dto.ProductDto;
import com.heliya.onlineshop.dto.ProductFilteredSearch;
import com.heliya.onlineshop.dto.ProductRequest;
import com.heliya.onlineshop.mapper.ProductMapper;
import com.heliya.onlineshop.model.Product;
import com.heliya.onlineshop.repository.ProductRepository;
import com.heliya.onlineshop.repository.projection.ProductIdAndPriceProjection;
import com.heliya.onlineshop.repository.specification.ProductSpecification;
import com.heliya.onlineshop.service.ProductService;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {
    private final ProductRepository repository;
    private final ProductMapper mapper;

    @Override
    @Transactional(readOnly = true)
    public List<ProductDto> findAll() {
        List<Product> products = repository.findAll();
        return mapper.toProductDtos(products);
    }

    @Override
    @Transactional
    public void save(ProductRequest productRequest) {
        Product product = mapper.toProduct(productRequest);
        repository.save(product);
    }

    @Override
    @Transactional(readOnly = true)
    public ProductDto findOne(Integer id) {
        Product product = repository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Not found product by id: " + id));

        return mapper.toProductDto(product);
    }

    @Override
    @Transactional(readOnly = true)
    public Set<ProductIdAndPriceProjection> findAllProductIdPriceByIds(Set<Integer> ids) {
        return repository.findAllProductIdAndPriceByIdIn(ids);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ProductDto> search(ProductFilteredSearch productFilteredSearch) {
        if (productFilteredSearch == null || productFilteredSearch.isEmpty())
            return findAll();

        Specification<Product> searchProductByFiltersSpecification = ProductSpecification
                .searchByFilters(productFilteredSearch.name(),
                        productFilteredSearch.price(),
                        productFilteredSearch.type());

        List<Product> products = repository.findAll(searchProductByFiltersSpecification);
        return mapper.toProductDtos(products);
    }
}
