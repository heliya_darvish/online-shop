package com.heliya.onlineshop.service.impl;

import com.heliya.onlineshop.dto.WalletResponse;
import com.heliya.onlineshop.enums.WalletTransactionAction;
import com.heliya.onlineshop.exception.PaymentAccessDeniedException;
import com.heliya.onlineshop.mapper.WalletMapper;
import com.heliya.onlineshop.model.Purchase;
import com.heliya.onlineshop.model.User;
import com.heliya.onlineshop.model.Wallet;
import com.heliya.onlineshop.model.WalletTransaction;
import com.heliya.onlineshop.repository.WalletRepository;
import com.heliya.onlineshop.repository.WalletTransactionRepository;
import com.heliya.onlineshop.security.SecurityContextUtil;
import com.heliya.onlineshop.service.WalletService;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class WalletServiceImpl implements WalletService {
    private final WalletRepository walletRepository;
    private final WalletTransactionRepository walletTransactionRepository;
    private final WalletMapper mapper;

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public void deChargeWallet(Purchase purchase) {
        Integer userId = purchase.getUser().getId();
        Wallet wallet = walletRepository
                .findForUpdateById(userId)
                .orElseThrow(() -> new EntityNotFoundException("Not found wallet by userId: " + userId));

        int ipgPrice = calculateIpgPrice(purchase);
        int balance = wallet.getBalance();
        if (balance < ipgPrice)
            throw new PaymentAccessDeniedException("Wallet has not enough balance");

        wallet.setBalance(balance - ipgPrice);
        walletRepository.save(wallet);

        WalletTransaction walletTransaction = mapper.toWalletTransaction(
                purchase, wallet, ipgPrice, WalletTransactionAction.DE_CHARGE);
        walletTransactionRepository.save(walletTransaction);
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public void chargeWallet(Purchase cancelledPurchase) {
        Integer userId = cancelledPurchase.getUser().getId();
        Wallet wallet = walletRepository.findForUpdateById(userId)
                .orElseThrow(() -> new EntityNotFoundException("Not found wallet by userId: " + userId));

        int ipgPrice = calculateIpgPrice(cancelledPurchase);
        int balance = wallet.getBalance();

        wallet.setBalance(balance + ipgPrice);
        walletRepository.save(wallet);

        WalletTransaction walletTransaction = mapper.toWalletTransaction(
                cancelledPurchase, wallet, ipgPrice, WalletTransactionAction.CHARGE);
        walletTransactionRepository.save(walletTransaction);
    }

    private int calculateIpgPrice(Purchase purchase) {
        return purchase.getPrice() + purchase.getTax();
    }

    @Override
    @Transactional(readOnly = true)
    public WalletResponse find() {
        Integer userId = SecurityContextUtil.getUserId();
        Wallet wallet = walletRepository.findByUserId(userId)
                .orElseThrow(() -> new EntityNotFoundException("Not found wallet by userId: " + userId));

        return new WalletResponse(wallet.getBalance());
    }

    @Override
    @Transactional
    public void saveEmptyWallet(User user) {
        Wallet wallet = mapper.toWallet(user);
        walletRepository.save(wallet);
    }

}
