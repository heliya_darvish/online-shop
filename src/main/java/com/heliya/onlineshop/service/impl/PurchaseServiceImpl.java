package com.heliya.onlineshop.service.impl;

import com.heliya.onlineshop.config.properties.AppProperties;
import com.heliya.onlineshop.dto.PurchaseRequest;
import com.heliya.onlineshop.enums.PurchaseStatus;
import com.heliya.onlineshop.exception.PaymentAccessDeniedException;
import com.heliya.onlineshop.mapper.PurchaseMapper;
import com.heliya.onlineshop.model.Purchase;
import com.heliya.onlineshop.dto.PurchaseResponse;
import com.heliya.onlineshop.repository.PurchaseRepository;
import com.heliya.onlineshop.repository.projection.ProductIdAndPriceProjection;
import com.heliya.onlineshop.security.SecurityContextUtil;
import com.heliya.onlineshop.service.PaymentService;
import com.heliya.onlineshop.service.ProductService;
import com.heliya.onlineshop.service.PurchaseService;
import com.heliya.onlineshop.service.WalletService;
import jakarta.persistence.EntityNotFoundException;
import jakarta.validation.ValidationException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PurchaseServiceImpl implements PurchaseService {
    private final PurchaseRepository repository;
    private final PurchaseMapper mapper;
    private final ProductService productService;
    private final WalletService walletService;
    private final PaymentService paymentService;
    private final AppProperties appProperties;

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public Purchase save(PurchaseRequest purchaseRequest, PurchaseStatus status) {
        Set<ProductIdAndPriceProjection> products = productService
                .findAllProductIdPriceByIds(purchaseRequest.productIds());

        validateRequestedProducts(purchaseRequest.productIds(), products);

        Integer price = calculateTotalPrice(products);
        Integer tax = calculateTax(price);

        Purchase purchase = mapper.toPurchase(products, status, price, tax);
        return repository.save(purchase);
    }

    private void validateRequestedProducts(Set<Integer> requestedProductIds,
                                           Set<ProductIdAndPriceProjection> products) {
        Optional<Integer> invalidRequestedProductId = requestedProductIds.stream()
                .filter(requestedProductId ->
                        products.stream()
                                .noneMatch(productIdAndPrice -> requestedProductId.equals(productIdAndPrice.getId())))
                .findAny();

        if (invalidRequestedProductId.isPresent())
            throw new ValidationException("invalid productId: " + invalidRequestedProductId.get());
    }

    private Integer calculateTotalPrice(Set<ProductIdAndPriceProjection> products) {
        return products.stream()
                .mapToInt(ProductIdAndPriceProjection::getPrice)
                .sum();
    }

    private Integer calculateTax(Integer price) {
        Integer taxPercent = appProperties.taxPercent();
        return Math.toIntExact(Math.round(price * ((double) taxPercent / 100.0)));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void cancel(Integer purchaseId) {
        Purchase purchase = repository.findById(purchaseId)
                .orElseThrow(() -> new EntityNotFoundException("Not found purchase by id: " + purchaseId));

        if (!SecurityContextUtil.getUserId().equals(purchase.getUser().getId()))
            throw new PaymentAccessDeniedException();

        if (PurchaseStatus.PAID.equals(purchase.getStatus()))
            walletService.chargeWallet(purchase);

        update(purchase, PurchaseStatus.CANCELED);
    }

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public void update(Purchase purchase, PurchaseStatus status) {
        purchase.setStatus(status);
        repository.save(purchase);
    }

    @Override
    @Transactional
    public void expirePurchases() {
        ZonedDateTime purchaseCreatedAtBefore = ZonedDateTime.now(ZoneId.of("Asia/Tehran"))
                .minusMinutes(appProperties.expirePurchasePeriodMinute());

        List<Purchase> shouldExpiredPurchases = repository.findAllByCreatedAtBeforeAndStatus(
                purchaseCreatedAtBefore, PurchaseStatus.NOT_PAID);
        if (ObjectUtils.isEmpty(shouldExpiredPurchases))
            return;

        shouldExpiredPurchases.forEach(purchase -> purchase.setStatus(PurchaseStatus.EXPIRED));
        repository.saveAll(shouldExpiredPurchases);

        Set<Integer> expiredPurchaseIds = shouldExpiredPurchases.stream()
                .map(Purchase::getId)
                .collect(Collectors.toSet());

        paymentService.expirePaymentsByPurchaseIds(expiredPurchaseIds);
    }

    @Override
    @Transactional(readOnly = true)
    public Purchase findById(Integer purchaseId) {
        return repository.findById(purchaseId)
                .orElseThrow(() -> new EntityNotFoundException("Not found purchase by id: " + purchaseId));
    }

    @Override
    public List<PurchaseResponse> findAllNotPaidPurchase() {
        List<Purchase> purchases = repository.findAllByStatus(PurchaseStatus.NOT_PAID);
        return mapper.toPurchaseResponses(purchases);
    }

}
