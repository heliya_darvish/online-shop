package com.heliya.onlineshop.service.impl;

import com.heliya.onlineshop.dto.UserRequest;
import com.heliya.onlineshop.mapper.UserMapper;
import com.heliya.onlineshop.model.User;
import com.heliya.onlineshop.repository.UserRepository;
import com.heliya.onlineshop.service.UserService;
import com.heliya.onlineshop.service.WalletService;
import jakarta.validation.ValidationException;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository repository;
    private final UserMapper mapper;
    private final WalletService walletService;
    private final PasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public void register(UserRequest userRequest) {
        boolean exists = repository.existsByUsername(userRequest.username());
        if (exists)
            throw new ValidationException("Username is repetitive");

        String encodedPassword = passwordEncoder.encode(userRequest.password());
        User user = mapper.toUser(userRequest, encodedPassword);
        User registerdUser = repository.save(user);
        walletService.saveEmptyWallet(registerdUser);
    }
}
