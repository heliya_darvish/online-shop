package com.heliya.onlineshop.service;

import com.heliya.onlineshop.dto.OnlinePurchaseResponse;
import com.heliya.onlineshop.dto.PurchaseRequest;
import com.heliya.onlineshop.enums.IpgResponse;

public interface BuyService {
    OnlinePurchaseResponse submitOnlinePurchase(PurchaseRequest purchaseRequest);

    OnlinePurchaseResponse submitOnlinePurchase(Integer purchaseId);

    void submitWalletPurchase(PurchaseRequest purchaseRequest);

    void submitWalletPurchase(Integer purchaseId);

    IpgResponse finalizePayment(Integer id);

}
