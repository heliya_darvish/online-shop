package com.heliya.onlineshop.service;

import com.heliya.onlineshop.dto.BasketResponse;

public interface BasketService {
    BasketResponse find();

    void addProduct(Integer productId);

    void removeProduct(Integer productId);

    void clearAfterLogin();

    void clearByUserId(Integer userId);
}
