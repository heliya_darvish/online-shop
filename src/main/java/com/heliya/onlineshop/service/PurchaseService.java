package com.heliya.onlineshop.service;

import com.heliya.onlineshop.dto.PurchaseRequest;
import com.heliya.onlineshop.enums.PurchaseStatus;
import com.heliya.onlineshop.model.Purchase;
import com.heliya.onlineshop.dto.PurchaseResponse;

import java.util.List;

public interface PurchaseService {
    Purchase save(PurchaseRequest purchaseRequest, PurchaseStatus status);

    void update(Purchase purchase, PurchaseStatus status);

    void cancel(Integer purchaseId);

    void expirePurchases();

    Purchase findById(Integer purchaseId);

    List<PurchaseResponse> findAllNotPaidPurchase();

}
