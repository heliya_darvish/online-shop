package com.heliya.onlineshop.service;

import com.heliya.onlineshop.dto.WalletResponse;
import com.heliya.onlineshop.model.Purchase;
import com.heliya.onlineshop.model.User;

public interface WalletService {
    void deChargeWallet(Purchase purchase);

    void chargeWallet(Purchase purchase);

    WalletResponse find();

    void saveEmptyWallet(User user);
}
