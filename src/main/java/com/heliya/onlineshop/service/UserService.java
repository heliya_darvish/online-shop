package com.heliya.onlineshop.service;

import com.heliya.onlineshop.dto.UserRequest;

public interface UserService {
    void register(UserRequest userRequest);
}
