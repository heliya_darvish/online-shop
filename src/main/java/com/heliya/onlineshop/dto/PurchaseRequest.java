package com.heliya.onlineshop.dto;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

import java.util.Set;

public record PurchaseRequest(@NotNull @NotEmpty Set<Integer> productIds) {
}
