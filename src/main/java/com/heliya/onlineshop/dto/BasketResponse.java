package com.heliya.onlineshop.dto;

import java.util.Set;

public record BasketResponse(Set<ProductDto> products) {
}
