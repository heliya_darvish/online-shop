package com.heliya.onlineshop.dto;

import com.heliya.onlineshop.enums.ProductType;
import com.heliya.onlineshop.model.Product;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PositiveOrZero;
import org.hibernate.validator.constraints.Length;

/**
 * DTO for {@link Product}
 */
public record ProductRequest(@NotNull @NotEmpty @Length(max = 50) String name,
                             @NotNull @PositiveOrZero Integer price,
                             ProductType type) {
}