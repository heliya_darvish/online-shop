package com.heliya.onlineshop.dto;

import com.heliya.onlineshop.enums.PurchaseStatus;
import com.heliya.onlineshop.model.Purchase;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

import java.util.Set;

/**
 * DTO for {@link Purchase}
 */
public record PurchaseResponse(@NotNull Integer id,
                               @NotNull PurchaseStatus status,
                               @NotNull Integer price,
                               Integer tax,
                               @NotNull @NotEmpty Set<PurchaseItemDto> items) {
}