package com.heliya.onlineshop.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.heliya.onlineshop.enums.ProductType;
import jakarta.validation.constraints.PositiveOrZero;
import org.hibernate.validator.constraints.Length;
import org.springframework.util.StringUtils;

public record ProductFilteredSearch(@Length(max = 50) String name,
                                    @PositiveOrZero Integer price,
                                    ProductType type) {

    @JsonIgnore
    public boolean isEmpty() {
        return !StringUtils.hasText(name) && price == null && type == null;
    }
}
