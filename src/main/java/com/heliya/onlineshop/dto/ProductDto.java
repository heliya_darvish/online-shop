package com.heliya.onlineshop.dto;

import com.heliya.onlineshop.enums.ProductType;
import com.heliya.onlineshop.model.Product;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PositiveOrZero;

/**
 * DTO for {@link Product}
 */
public record ProductDto(@NotNull Integer id,
                         @NotNull @NotEmpty String name,
                         @NotNull @PositiveOrZero Integer price,
                         ProductType type) {
}