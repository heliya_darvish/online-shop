package com.heliya.onlineshop.dto;

public record WalletResponse(int balance) {
}
