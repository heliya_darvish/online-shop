package com.heliya.onlineshop.dto;

import jakarta.validation.constraints.NotNull;

public record OnlinePurchaseResponse(@NotNull Integer paymentId) {
}
