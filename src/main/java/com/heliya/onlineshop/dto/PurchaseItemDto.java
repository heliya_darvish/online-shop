package com.heliya.onlineshop.dto;

import com.heliya.onlineshop.model.PurchaseItem;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PositiveOrZero;

/**
 * DTO for {@link PurchaseItem}
 */
public record PurchaseItemDto(@NotNull Integer productId,
                              @NotNull @PositiveOrZero Integer price) {
}