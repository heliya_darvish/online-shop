package com.heliya.onlineshop.dto;

import jakarta.validation.constraints.NotBlank;
import org.hibernate.validator.constraints.Length;

/**
 * DTO for {@link com.heliya.onlineshop.model.User}
 */
public record UserRequest(@NotBlank @Length(max = 50) String name,
                          @NotBlank @Length(max = 30) String username,
                          @NotBlank @Length(max = 20) String password) {
}