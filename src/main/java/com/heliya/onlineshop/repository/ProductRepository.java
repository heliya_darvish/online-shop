package com.heliya.onlineshop.repository;

import com.heliya.onlineshop.model.Product;
import com.heliya.onlineshop.repository.projection.ProductIdAndPriceProjection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Set;

public interface ProductRepository extends JpaRepository<Product, Integer>, JpaSpecificationExecutor<Product> {

    Set<ProductIdAndPriceProjection> findAllProductIdAndPriceByIdIn(Set<Integer> ids);
}
