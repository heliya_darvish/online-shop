package com.heliya.onlineshop.repository.specification;

import com.heliya.onlineshop.enums.ProductType;
import com.heliya.onlineshop.model.Product;
import jakarta.persistence.criteria.Predicate;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class ProductSpecification {
    private ProductSpecification() {
    }

    public static Specification<Product> searchByFilters(String name, Integer price, ProductType type) {
        return (root, query, builder) -> {
            List<Predicate> predicates = new ArrayList<>();

            if (StringUtils.hasText(name)) {
                predicates.add(builder.like(root.get("name"), "%" + name + "%"));
            }

            if (price != null) {
                predicates.add(builder.equal(root.get("price"), price));
            }

            if (type != null) {
                predicates.add(builder.equal(root.get("type"), type));
            }

            return builder.and(predicates.toArray(new Predicate[0]));
        };
    }

}
