package com.heliya.onlineshop.repository;

import com.heliya.onlineshop.enums.PurchaseStatus;
import com.heliya.onlineshop.model.Purchase;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.ZonedDateTime;
import java.util.List;

public interface PurchaseRepository extends JpaRepository<Purchase, Integer> {
    List<Purchase> findAllByCreatedAtBeforeAndStatus(ZonedDateTime createdAtBefore, PurchaseStatus status);

    @EntityGraph(
            type = EntityGraph.EntityGraphType.FETCH,
            value = "purchase-item-entity-graph"
    )
    List<Purchase> findAllByStatus(PurchaseStatus status);
}
