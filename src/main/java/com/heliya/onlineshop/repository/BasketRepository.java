package com.heliya.onlineshop.repository;

import com.heliya.onlineshop.model.Basket;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface BasketRepository extends JpaRepository<Basket, Integer> {

    @EntityGraph(
            type = EntityGraph.EntityGraphType.FETCH,
            value = "basket-product-entity-graph"
    )
    Optional<Basket> findByUserId(Integer userId);
}
