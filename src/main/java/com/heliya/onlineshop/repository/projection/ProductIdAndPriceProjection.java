package com.heliya.onlineshop.repository.projection;

public interface ProductIdAndPriceProjection {
    Integer getId();

    Integer getPrice();
}
