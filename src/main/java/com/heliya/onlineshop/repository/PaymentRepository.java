package com.heliya.onlineshop.repository;

import com.heliya.onlineshop.enums.PaymentStatus;
import com.heliya.onlineshop.model.Payment;
import jakarta.validation.constraints.NotNull;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface PaymentRepository extends JpaRepository<Payment, Integer> {

    @EntityGraph(
            type = EntityGraph.EntityGraphType.FETCH,
            value = "payment-purchase-entity-graph"
    )
    Optional<Payment> findWithPurchaseById(Integer id);

    List<Payment> findAllByPurchaseIdInAndStatus(Collection<Integer> purchaseIds, @NotNull PaymentStatus status);
}
