package com.heliya.onlineshop.enums;

public enum PaymentStatus {
    INITIATED,
    SUCCESS,
    FAILED,
    EXPIRED
}
