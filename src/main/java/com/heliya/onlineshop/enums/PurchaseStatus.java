package com.heliya.onlineshop.enums;

public enum PurchaseStatus {
    NOT_PAID,
    PAID,
    CANCELED,
    EXPIRED
}
