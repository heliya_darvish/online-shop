package com.heliya.onlineshop.enums;

public enum ProductType {
    CLOTHES,
    BOOK,
    MEDICINE
}
