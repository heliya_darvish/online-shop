package com.heliya.onlineshop.exception;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.ValidationException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.resource.NoResourceFoundException;

import java.util.*;

import static java.util.Optional.ofNullable;

@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler extends Exception {

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ApiCallError<T> {
        private String message;
        private List<T> details;
    }

    @ExceptionHandler(AuthenticationException.class)
    public ResponseEntity<ApiCallError<String>> handleAuthenticationException(HttpServletRequest request,
                                                                              AuthenticationException ex) {
        log.error("AuthenticationException {}\n", request.getRequestURI(), ex);
        String message = ex.getMessage() != null ? ex.getMessage() : "";
        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .body(new ApiCallError<>(message, null));
    }

    @ExceptionHandler(NoResourceFoundException.class)
    public ResponseEntity<ApiCallError<String>> handleNoResourceFoundException(HttpServletRequest request,
                                                                               NoResourceFoundException ex) {
        log.error("NoResourceFoundException {}\n", request.getRequestURI(), ex);
        String message = ex.getMessage() != null ? ex.getMessage() : "";
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(new ApiCallError<>(message, null));
    }

    @ExceptionHandler(ValidationException.class)
    public ResponseEntity<ApiCallError<String>> handleValidationException(HttpServletRequest request,
                                                                          ValidationException ex) {
        log.error("ValidationException {}\n", request.getRequestURI(), ex);
        String message = ex.getMessage() != null ? ex.getMessage() : "";
        return ResponseEntity
                .badRequest()
                .body(new ApiCallError<>(message, null));
    }

    @ExceptionHandler(HttpMessageConversionException.class)
    public ResponseEntity<ApiCallError<String>> handleHttpMessageConversionException(HttpServletRequest request,
                                                                                     HttpMessageConversionException ex) {
        log.error("HttpMessageConversionException {}\n", request.getRequestURI(), ex);
        String message = ex.getMessage() != null ? ex.getMessage() : "";
        return ResponseEntity
                .badRequest()
                .body(new ApiCallError<>(message, null));
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    public ResponseEntity<ApiCallError<String>> handleMissingServletRequestParameterException(HttpServletRequest request,
                                                                                              MissingServletRequestParameterException ex) {
        log.error("handleMissingServletRequestParameterException {}\n", request.getRequestURI(), ex);
        return ResponseEntity
                .badRequest()
                .body(new ApiCallError<>("Missing request parameter", List.of(ex.getMessage())));
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<ApiCallError<Map<String, String>>> handleMethodArgumentTypeMismatchException(
            HttpServletRequest request, MethodArgumentTypeMismatchException ex) {
        log.error("handleMethodArgumentTypeMismatchException {}\n", request.getRequestURI(), ex);

        Map<String, String> details = new HashMap<>();
        details.put("paramName", ex.getName());
        details.put("paramValue", ofNullable(ex.getValue()).map(Object::toString).orElse(""));
        details.put("errorMessage", ex.getMessage());

        return ResponseEntity
                .badRequest()
                .body(new ApiCallError<>("Method argument type mismatch", List.of(details)));
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ApiCallError<Map<String, String>>> handleMethodArgumentNotValidException(
            HttpServletRequest request, MethodArgumentNotValidException ex) {
        log.error("handleMethodArgumentNotValidException {}\n", request.getRequestURI(), ex);

        List<Map<String, String>> details = new ArrayList<>();
        ex.getBindingResult()
                .getFieldErrors()
                .forEach(fieldError -> {
                    Map<String, String> detail = new HashMap<>();
                    detail.put("objectName", fieldError.getObjectName());
                    detail.put("field", fieldError.getField());
                    detail.put("rejectedValue", String.valueOf(fieldError.getRejectedValue()));
                    detail.put("errorMessage", fieldError.getDefaultMessage());
                    details.add(detail);
                });

        return ResponseEntity
                .badRequest()
                .body(new ApiCallError<>("Method argument validation failed", details));
    }


    @ExceptionHandler(DataAccessException.class)
    public ResponseEntity<ApiCallError<String>> handleDataAccessException(HttpServletRequest request,
                                                                          DataAccessException ex) {
        log.error("handleDataAccessException {}\n", request.getRequestURI(), ex);
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(new ApiCallError<>("Internal server error", Collections.emptyList()));
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ApiCallError<String>> handleInternalServerError(HttpServletRequest request, Exception ex) {
        log.error("handleInternalServerError {}\n", request.getRequestURI(), ex);
        String message = ex.getMessage() != null ? ex.getMessage() : "";
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(new ApiCallError<>("Internal server error", List.of(message)));
    }

    @ExceptionHandler(PaymentAccessDeniedException.class)
    public ResponseEntity<ApiCallError<String>> handlePaymentAccessDeniedException(HttpServletRequest request,
                                                                                   PaymentAccessDeniedException ex) {
        log.error("handlePaymentAccessDeniedException {}\n", request.getRequestURI(), ex);
        String message = ex.getMessage() != null ? ex.getMessage() : "";
        return ResponseEntity
                .status(HttpStatus.FORBIDDEN)
                .body(new ApiCallError<>("Payment access denied", List.of(message)));
    }

}
