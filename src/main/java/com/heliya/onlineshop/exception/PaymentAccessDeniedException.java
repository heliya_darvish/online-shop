package com.heliya.onlineshop.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class PaymentAccessDeniedException extends RuntimeException {
    public PaymentAccessDeniedException(String message) {
        super(message);
    }
}
