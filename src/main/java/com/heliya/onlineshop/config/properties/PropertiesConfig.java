package com.heliya.onlineshop.config.properties;

import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.Configuration;

@ConfigurationPropertiesScan(basePackageClasses = PropertiesConfig.class)
@Configuration
public class PropertiesConfig {
}
