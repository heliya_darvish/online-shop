package com.heliya.onlineshop.config.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("app.properties")
public record AppProperties(Integer taxPercent,
                            int expirePurchasePeriodMinute,
                            ExpirePurchaseSchedule expirePurchaseSchedule) {

    public record ExpirePurchaseSchedule(int rateMilliseconds, int initDelayMilliseconds) {
    }
}
