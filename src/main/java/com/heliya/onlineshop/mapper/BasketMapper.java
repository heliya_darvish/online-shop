package com.heliya.onlineshop.mapper;

import com.heliya.onlineshop.dto.BasketResponse;
import com.heliya.onlineshop.model.Basket;
import com.heliya.onlineshop.model.Product;
import org.mapstruct.BeanMapping;
import org.mapstruct.Condition;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.Collection;

@Mapper(uses = ProductMapper.class)
public interface BasketMapper extends LazyLoadingAwareMapper {
    @Condition
    default boolean isLazyLoadedProduct(Collection<Product> sourceCollection) {
        return isInitialized(sourceCollection);
    }

    @Mapping(target = "products", expression = "java(java.util.Set.of(toProduct(productId)))")
    @Mapping(target = "user.id", source = "userId")
    Basket toBasket(Integer userId, Integer productId);

    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "productId")
    Product toProduct(Integer productId);

    @Mapping(target = "products", defaultExpression = "java(java.util.Collections.emptySet())")
    BasketResponse toBasketResponse(Basket basket);
}
