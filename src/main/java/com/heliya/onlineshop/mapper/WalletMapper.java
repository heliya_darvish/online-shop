package com.heliya.onlineshop.mapper;

import com.heliya.onlineshop.enums.WalletTransactionAction;
import com.heliya.onlineshop.model.Purchase;
import com.heliya.onlineshop.model.User;
import com.heliya.onlineshop.model.Wallet;
import com.heliya.onlineshop.model.WalletTransaction;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface WalletMapper {
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "issuedAt", ignore = true)
    @Mapping(target = "balance", source = "transactionBalance")
    WalletTransaction toWalletTransaction(Purchase purchase,
                                          Wallet wallet,
                                          int transactionBalance,
                                          WalletTransactionAction action);

    @Mapping(target = "userId", source = "id")
    @Mapping(target = "user", source = "user")
    @Mapping(target = "balance", constant = "0")
    Wallet toWallet(User user);

    @Mapping(target = "userId", source = "userId")
    @Mapping(target = "user.id", source = "userId")
    @Mapping(target = "balance", constant = "0")
    Wallet toWallet(Integer userId);
}
