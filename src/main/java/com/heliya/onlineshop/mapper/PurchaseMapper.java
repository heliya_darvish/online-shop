package com.heliya.onlineshop.mapper;

import com.heliya.onlineshop.enums.PurchaseStatus;
import com.heliya.onlineshop.model.Purchase;
import com.heliya.onlineshop.model.PurchaseItem;
import com.heliya.onlineshop.dto.PurchaseItemDto;
import com.heliya.onlineshop.dto.PurchaseResponse;
import com.heliya.onlineshop.repository.projection.ProductIdAndPriceProjection;
import org.mapstruct.Condition;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.Collection;
import java.util.List;
import java.util.Set;

@Mapper
public interface PurchaseMapper extends LazyLoadingAwareMapper {

    @Condition
    default boolean isLazyLoadedPurchaseItem(Collection<PurchaseItem> sourceCollection) {
        return isInitialized(sourceCollection);
    }

    @Mapping(target = "user", ignore = true)
    @Mapping(target = "modifiedAt", ignore = true)
    @Mapping(target = "items", source = "products")
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "id", ignore = true)
    Purchase toPurchase(Set<ProductIdAndPriceProjection> products,
                        PurchaseStatus status,
                        Integer price,
                        Integer tax);

    Set<PurchaseItem> toPurchaseItems(Set<ProductIdAndPriceProjection> products);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "product.id", source = "id")
    PurchaseItem toPurchaseItem(ProductIdAndPriceProjection product);

    List<PurchaseResponse> toPurchaseResponses(List<Purchase> purchases);

    PurchaseResponse toPurchaseResponse(Purchase purchase);

    Set<PurchaseItemDto> toPurchaseItemDtos(Set<PurchaseItem> items);

    @Mapping(target = "productId", source = "product.id")
    PurchaseItemDto toPurchaseItemDto(PurchaseItem item);
}
