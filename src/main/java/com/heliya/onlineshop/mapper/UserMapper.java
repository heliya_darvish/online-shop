package com.heliya.onlineshop.mapper;

import com.heliya.onlineshop.dto.UserRequest;
import com.heliya.onlineshop.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface UserMapper {
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "password", source = "password")
    User toUser(UserRequest userRequest, String password);
}
