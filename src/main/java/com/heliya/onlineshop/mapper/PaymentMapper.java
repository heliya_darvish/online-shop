package com.heliya.onlineshop.mapper;

import com.heliya.onlineshop.enums.PaymentStatus;
import com.heliya.onlineshop.model.Payment;
import com.heliya.onlineshop.model.Purchase;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface PaymentMapper {
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "modifiedAt", ignore = true)
    @Mapping(target = "user", ignore = true)
    @Mapping(target = "status", source = "paymentStatus")
    @Mapping(target = "purchase", source = "purchase")
    @Mapping(target = "ipgPrice", expression = "java(calculateIpgPrice(purchase))")
    Payment toPayment(Purchase purchase, PaymentStatus paymentStatus);

    default Integer calculateIpgPrice(Purchase purchase) {
        return purchase.getPrice() + purchase.getTax();
    }
}
