package com.heliya.onlineshop.mapper;

import org.hibernate.Hibernate;

import java.util.Collection;

public interface LazyLoadingAwareMapper {
    default boolean isInitialized(Collection<?> sourceCollection) {
        return Hibernate.isInitialized(sourceCollection);
    }
}
