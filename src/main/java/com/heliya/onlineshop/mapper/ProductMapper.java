package com.heliya.onlineshop.mapper;

import com.heliya.onlineshop.dto.ProductDto;
import com.heliya.onlineshop.dto.ProductRequest;
import com.heliya.onlineshop.model.Product;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;
import java.util.Set;

@Mapper
public interface ProductMapper {
    List<ProductDto> toProductDtos(List<Product> products);

    Set<ProductDto> toProductDtos(Set<Product> products);

    @Mapping(target = "id", ignore = true)
    Product toProduct(ProductRequest productRequest);

    ProductDto toProductDto(Product product);
}
