package com.heliya.onlineshop.schedule;

import com.heliya.onlineshop.service.PurchaseService;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class Schedule {
    private final PurchaseService purchaseService;

    @Scheduled(fixedRateString = "${app.properties.expire-purchase-schedule.rate-milliseconds}",
            initialDelayString = "${app.properties.expire-purchase-schedule.init-delay-milliseconds}")
    public void scheduleExpirePurchasesTask() {
        purchaseService.expirePurchases();
    }

}
