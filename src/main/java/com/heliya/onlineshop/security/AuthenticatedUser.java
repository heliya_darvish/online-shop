package com.heliya.onlineshop.security;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.userdetails.User;

import java.util.Collections;
import java.util.Objects;

@Getter
@Setter
public class AuthenticatedUser extends User {
    private Integer id;

    public AuthenticatedUser(Integer id, String username, String password) {
        super(username, password, Collections.emptyList());
        this.id = id;
    }

    public com.heliya.onlineshop.model.User getUser() {
        com.heliya.onlineshop.model.User user = new com.heliya.onlineshop.model.User();
        user.setId(id);
        return user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;

        if (o == null || getClass() != o.getClass())
            return false;

        if (!super.equals(o))
            return false;

        AuthenticatedUser that = (AuthenticatedUser) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), id);
    }
}
