package com.heliya.onlineshop.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Optional;

public class SecurityContextUtil {
    private SecurityContextUtil() {
    }

    public static Integer getUserId() {
        return getUserIdIfExists()
                .orElseThrow(() -> new UsernameNotFoundException("Unable to extract username from security context"));
    }

    public static Optional<Integer> getUserIdIfExists() {
        return Optional.ofNullable(SecurityContextHolder.getContext())
                .map(SecurityContext::getAuthentication)
                .filter(Authentication::isAuthenticated)
                .map(Authentication::getPrincipal)
                .map(principal -> ((AuthenticatedUser) principal).getId());
    }

}
