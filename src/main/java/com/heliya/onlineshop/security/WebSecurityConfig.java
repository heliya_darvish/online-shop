package com.heliya.onlineshop.security;

import com.heliya.onlineshop.model.User;
import com.heliya.onlineshop.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.actuate.autoconfigure.endpoint.web.WebEndpointProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.CsrfConfigurer;
import org.springframework.security.config.annotation.web.configurers.HeadersConfigurer;
import org.springframework.security.config.annotation.web.configurers.SessionManagementConfigurer;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.SecurityFilterChain;

@Slf4j
@Configuration
@EnableWebSecurity
public class WebSecurityConfig {

    @Bean
    SecurityFilterChain filterChain(HttpSecurity http, WebEndpointProperties webEndpointProperties) throws Exception {
        http.cors(Customizer.withDefaults());
        http.csrf(CsrfConfigurer<HttpSecurity>::disable);
        http.headers(headers -> headers.frameOptions(HeadersConfigurer.FrameOptionsConfig::sameOrigin));

        http.logout(logout ->
                logout.deleteCookies("JSESSIONID"));

        http.sessionManagement(sessionManagementConfigurer ->
                sessionManagementConfigurer
                        .sessionFixation(SessionManagementConfigurer.SessionFixationConfigurer::newSession));

        http.authorizeHttpRequests(authorizationManagerRequestMatcherRegistry ->
                authorizationManagerRequestMatcherRegistry
                        .requestMatchers("/h2-console/**", "/swagger-ui/**", "/api-docs/**",
                                webEndpointProperties.getBasePath() + "/**",
                                "/products/search",
                                "/users",
                                "/finalize-payment/{paymentId}"
                        ).permitAll());

        http.authorizeHttpRequests(authorizationManagerRequestMatcherRegistry ->
                authorizationManagerRequestMatcherRegistry
                        .requestMatchers(HttpMethod.GET, "/products**", "/products/**")
                        .permitAll());

        http.authorizeHttpRequests(authorizationManagerRequestMatcherRegistry ->
                authorizationManagerRequestMatcherRegistry
                        .anyRequest()
                        .fullyAuthenticated());

        http.formLogin(formLoginConfigurer -> {
            formLoginConfigurer.successHandler((request, response, authentication) ->
                    response.setStatus(HttpStatus.NO_CONTENT.value()));

            formLoginConfigurer.failureHandler((request, response, authenticationException) ->
                    response.setStatus(HttpStatus.UNAUTHORIZED.value()));
        });

        DefaultSecurityFilterChain defaultSecurityFilterChain = http.build();
        log.debug("Config SecurityFilterChain successfully: {}", defaultSecurityFilterChain);
        return defaultSecurityFilterChain;
    }

    @Bean
    UserDetailsService userDetailsService(UserRepository userRepository) {
        return username -> {
            User user = userRepository.findByUsername(username)
                    .orElseThrow(() -> new UsernameNotFoundException("User not found with username: " + username));

            return new AuthenticatedUser(user.getId(), user.getUsername(), user.getPassword());
        };
    }

    @Bean
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
