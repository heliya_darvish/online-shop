FROM maven:3.9.6-eclipse-temurin-21-alpine AS Builder
WORKDIR /app
COPY . .
RUN --mount=type=cache,target=/root/.m2 mvn clean package

FROM eclipse-temurin:21-jre-alpine
COPY --from=builder /app/target/online-shop-*.jar online-shop.jar
ENV JAVA_OPTS ""
EXPOSE 8080
ENTRYPOINT java ${JAVA_OPTS} -jar /online-shop.jar